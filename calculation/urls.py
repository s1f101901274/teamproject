from django.urls import path
from . import views

urlpatterns = [
    path("",views.top,name='top'),
    path("keisan/",views.keisan,name='keisan'),
    path("shift/<int:workplace_id>/", views.shift, name="shift"), #シフトの詳細
    path("shift/<int:schedule_id>/remove/", views.shift_remove, name="shift_remove"), #シフトの削除
    path("registration/<int:workplace_id>/",views.registration, name='registration'),
    path("delete/<int:shift_id>/",views.delete, name='delete'),
    path("update/<int:shift_id>/",views.update, name='update'),
    path("test/",views.test),
    path("baito/",views.baito,name="baito"), #バイト先の登録
    path("baito/<int:workplace_id>/remove/", views.baito_remove, name="baito_remove")
]
