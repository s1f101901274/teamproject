import datetime
from django.db import models
from django.utils import timezone


class Workplsce(models.Model):
    """バイト先の情報"""
    name = models.CharField(max_length=50)
    wage = models.IntegerField(default=0)

class Schedule(models.Model):
    """スケジュール"""
    workplace = models.ForeignKey(Workplsce, on_delete=models.CASCADE)
    start_time = models.TimeField(default=datetime.time(7, 0, 0))
    end_time = models.TimeField( default=datetime.time(7, 0, 0))
    date = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField(default=timezone.now)
    def hours(self):
        end_minutes = self.end_time.hour*60 + self.end_time.minute
        start_minutes = self.start_time.hour*60 + self.start_time.minute
        end_minutes = end_minutes if (end_minutes >= start_minutes ) else end_minutes+24*60
        return (end_minutes - start_minutes) / 60
