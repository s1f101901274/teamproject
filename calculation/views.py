from django.shortcuts import render, redirect
from calculation.models import Schedule,Workplsce
import datetime

# Create your views here.
def top(request):
    """トップ画面(一覧画面)"""
    """日記一覧画面"""
    Workplsces = Workplsce.objects.all()
    context = {
        'workplsces' : Workplsces,
    }
    return render(request, 'calculation/top.html', context)

def shift(request, workplace_id):
    workplace_data = Workplsce.objects.get(pk=workplace_id)
    shifts = Schedule.objects.all().filter(workplace=workplace_data)
    salary_sum = 0
    for shift in shifts:
        salary_sum += workplace_data.wage * shift.hours()

    context = {
        'workplace_data' : workplace_data,
        'shifts' : shifts,
        'salary_sum':int(salary_sum),
    }
    return render(request, 'calculation/shift.html', context)

def shift_remove(request, schedule_id):
    terget_shift = Schedule.objects.get(pk=schedule_id)
    terget_shift.delete()
    return redirect('top')

def keisan(request):
    """一覧画面"""
    schedules = Schedule.objects.order_by("-date")
    res = []
    month_salary = 0
    total_time = 0
    for schedule in schedules:
        wage = schedule.workplace.wage # 時給
        time = schedule.hours()
        res.append({
            "workplace_name":schedule.workplace.name,
            "date":schedule.date,
            "starttime":schedule.start_time,
            "endtime":schedule.end_time,
            "time": time,
            "salary":int(wage*time),
            "id":schedule.id,
        })
        month_salary += int(wage*time)
        total_time += time
    context = {
        "context":res,
        "month_salary":month_salary,
        "total_time":total_time,
        }
    return render(request,'calculation/keisan.html', context)

def registration(request, workplace_id):
    """登録画面"""
    workplace_data = Workplsce.objects.get(pk=workplace_id)
    if request.method == 'POST':
        time = Schedule.objects.create(
            workplace = workplace_data,
            start_time = request.POST['start_time'],
            end_time = request.POST['end_time'],
            date = request.POST['date']
        )
        return redirect('shift',workplace_id)
    else:
        context = {
            "workplace_data" : workplace_data,
        }
        return render(request, 'calculation/registration.html', context)

def test(request):
    """test"""
    return render(request,'calculation/baito.html')

def baito(request):
    """新規登録画面"""
    if request.method == 'POST':
        baito1 = Workplsce.objects.create(
            name = request.POST['baito_name'],
            wage = request.POST['wage']
        )
        return redirect('top')
    return render(request,'calculation/baito.html')

def baito_remove(request, workplace_id):
    workplace_data = Workplsce.objects.get(pk=workplace_id)
    workplace_data.delete()
    return redirect("top")

def wage(request):
    """給料計算"""
    

def delete(request, shift_id):
    """削除画面"""
    shift = Schedule.objects.get(pk=shift_id)
    shift.delete()
    return redirect('keisan')

def update(request, shift_id):
    """更新画面"""
    shift = Schedule.objects.get(pk=shift_id)
    if request.method == 'GET':
        context = {
            "shift":shift,
        }
        return render(request, 'calculation/update.html', context)

    elif request.method == 'POST':
        shift.start_time= request.POST['start_time']
        shift.end_time = request.POST['end_time']
        shift.save()
        return redirect('keisan')
